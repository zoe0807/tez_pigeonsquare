package view;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import controller.Main;
import javafx.application.Platform;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import model.Square;
import model.UpdateEventListener;
import model.Enum.ActionType;
import model.Coords;
import model.Food;
import model.Item;
import model.Pigeon;
import model.Repulsive;

public class UIManager implements UpdateEventListener {
	
	private Square square;
	
	public UIManager(Square square) {
		this.square = square;
		this.square.setUpdateEventListener(this);
	}
	
	public void loadImage(String path) {
		ImageView imageView;
		Image image;
		try {
			image = new Image(new FileInputStream(path));
	        imageView = new ImageView();
	        imageView.setImage(image);
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Node addImage(Item item) {
		ImageView imageView = null;
		Image image;
		String path = "";
		if(item instanceof Food)
			path = "resources/food.gif";
		else if(item instanceof Pigeon)
			path = "resources/pigeon.png";
		else if(item instanceof Repulsive)
			path = "resources/bomb.png";
	
		try {
			image = new Image(new FileInputStream(path));
	        imageView = new ImageView();
	        imageView.setImage(image);
	        
	        imageView.setFitWidth(32);
	        imageView.setFitHeight(32);
	        
	        int pX = (int) (imageView.getFitWidth() / 2);
            int pY = (int) (imageView.getFitHeight() / 2);
            int posX = item.getCoords().getX();
            int posY = item.getCoords().getY();
            
            item.setCoords(new Coords(posX - pX, posY - pY));
            
            imageView.setX(item.getCoords().getX());
            imageView.setY(item.getCoords().getY());
            
            imageView.setPreserveRatio(true);
	        
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		item.setImageView(imageView);
		return imageView;
	}
	
	public void updateImage(Item item) {
		Platform.runLater(() -> {
			try {
				Image image = new Image(new FileInputStream("resources/food_notfresh.gif"));
				item.getImageView().setImage(image);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		});
	}
	
	public void deleteImage(Item item) {
		System.out.println("DELETE IMAGE");
		//Platform.runLater(() -> {
			if(item != null) {
				System.out.println("remove ---> " + item);
				item.getImageView().setImage(null);
				item.setImageView(null);
			}
			else {
				System.out.println("----------> ITEM = null !!!!");
			}
		//});
		
	}
	
	public void updatePosition(Item item) {
		Platform.runLater(() -> {
			item.getImageView().setX(item.getCoords().getX());
			item.getImageView().setY(item.getCoords().getY());
		});
	}

	@Override
	public void onSquareUpdate(Item item, ActionType actiontype) {
		//Platform.runLater(() -> {
			if(item instanceof Food) {
				switch(actiontype) {
					case Create : Main.root.getChildren().add(addImage(item)); break;
					case Delete : deleteImage(item); break;//Main.root.getChildren().remove(item.getImageView()); break;
					case Update : updateImage(item); break;
					default : break;
				}			
			}
			else if(item instanceof Pigeon) {
				switch(actiontype) {
					case Create : Main.root.getChildren().add(addImage(item)); break;
					case Delete : break;
					case Update : updatePosition(item); break;
					default : break;
				}	
			}
			else if(item instanceof Repulsive) {
				switch(actiontype) {
					case Create : Main.root.getChildren().add(addImage(item)); break;
					case Delete : deleteImage(item); break;
					case Update : break;
					default : break;
				}	
			}
		//});
	}
}
