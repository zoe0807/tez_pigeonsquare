package view;

import model.Coords;
import model.Food;
import model.Pigeon;

public class Interface {
    public final static Integer  qtPigeons = 10;
    public final static Integer longueurMax = 100;
    public final static Integer largeurMax = 100;

    public static void main(String [ ] args){
        Food food = null;
        Thread[] pigeonsThreads = new Thread[qtPigeons];
        ThreadGroup tg = new ThreadGroup("pigeonsThreads");
        for(int i = 0; i < qtPigeons; i++)
        	pigeonsThreads[i] = new Thread(tg, new Pigeon(new Coords((int) (Math.random() * longueurMax), (int) (Math.random() * largeurMax)), null), "p" + i);
        
    }
}
