package model;

import java.util.Vector;

import model.Enum.ActionType;

public class Pigeon extends Item {
	
	private Integer speed;
	private Integer eatenFood;
	private Square square;
	
	public Pigeon(Coords coords, Square square) {
		this.setCoords(coords);
		this.eatenFood = 0;
		this.speed = 2;
		this.square = square;
	}
	
	public int getEatenFood() {
		return this.eatenFood; 
	}

	public Food move(Food food){
		//System.out.println("Trying to move to [" + food.getCoords().getX() + ";" + food.getCoords().getY()+"]");
		move(food.getCoords().getX(), food.getCoords().getY());
		return food;
	}

	public void move(Repulsive repulsive){
		Integer pigeonx = this.getCoords().getX();
		Integer pigeony = this.getCoords().getY();
		Integer repulsivex = repulsive.getCoords().getX();
		Integer repulsivey = repulsive.getCoords().getY();

		// Si repulsif est plus grand que pigeon en X/Y alors le pigeon recule sinon il avance = le pigeon s'eloigne du repulsif
		Integer x = (repulsivex >= pigeonx ) ? pigeonx-repulsivex : pigeonx+repulsivex;
		Integer y = (repulsivey >= pigeony ) ? pigeony-repulsivey : pigeony+repulsivey;
		move(x,y);
	}

	private void move(int x, int y){
		Integer pigeonx = this.getCoords().getX();
		Integer pigeony = this.getCoords().getY();
		Integer itemx = x;
		Integer itemy = y;
		//System.out.println("PIGEON EN [" + this.getCoords().getX() + ";" + this.getCoords().getY()+"]");
		// cherche la distance (resp) en x et en y entre le pigeon et l'objet
		double distx = itemx - pigeonx;
		double disty = itemy - pigeony;

		// calcul la distance la plus courte entre le pigeon et l'objet
		double distttl = distance(this, x, y);
		distttl = (distttl == 0) ? 1 : distttl;
		//System.out.println("DISTANCE : " + distttl);
		// defini la vitesse de déplacement sur chaque axe
		distx *= speed/distttl;
		disty *= speed/distttl;

		// Si le pigeon dépace en X la food dans son mouvement, il s'arrète dessus, sinon il avance vers elle.
		if((pigeonx < itemx && pigeonx+distx >= itemx )
				|| (pigeonx > itemx && pigeonx+distx <= itemx ) )
			this.getCoords().setX(itemx);
		else {
			Integer xValue = (int)Math.round((pigeonx+distx));
			if(xValue > square.getxSize()- 20)
				xValue = square.getxSize() - 20;
			else if(xValue < 0)
				xValue = 0;
			this.getCoords().setX(xValue);
		}

		// Si le pigeon dépace en Y la food dans son mouvement, il s'arrète dessus, sinon il avance vers elle.
		if((pigeony < itemy && pigeony+disty >= itemy )
				|| (pigeony > itemy && pigeony+disty <= itemy ) )
			this.getCoords().setY(itemy);
		else {
			Integer yValue = (int)Math.round((pigeony+disty));
			if(yValue > square.getySize()- 20)
				yValue = square.getySize() - 20;
			else if(yValue < 0)
				yValue = 0;
			this.getCoords().setY(yValue);
		}
	}

	public void run(){
		while(isRunning){

		    // Tant qu'il existe des repulsives
            while(square.isRepulsiveHere()){
            	System.out.println("REPULSIVE DETECTED");
                try {
                    move(square.getCloseRepulsive(this));
                    square.update(this, ActionType.Update);
                    
                    Thread.sleep(10);
                } catch (Exception e) {
                    e.getMessage();
                }
            }

            // Tant qu'il existe de la food et qu'il n'y a pas de repulsive
			while(square.isFoodHere() && !square.isRepulsiveHere()){
				/*try {
					if(distance(this, square.getCloseFood(this).getCoords().getX(), square.getCloseFood(this).getCoords().getY()) == 0)
						break;
				} catch (Exception e1) {
					e1.printStackTrace();
					System.out.println("Exception : " + e1);
				}*/
			    try {
			        // Le pigeon se déplace vers la nourriture la plus proche
			    	
			        Food targetedFood = move(square.getCloseFood(this));
			        //System.out.println(targetedFood);
			        
			        // Si le pigeon est sur la nourriture et qu'elle est encore fresh, il la mange
			        if(distance(this, targetedFood.getCoords().getX(), targetedFood.getCoords().getY()) == 0 && targetedFood.isFresh()){
			            targetedFood.manger();
			            eatenFood++;
			        }
			        square.update(this, ActionType.Update);
			        
			    }
			    catch (Exception e){
			        e.getMessage();
			    }
			    
			    try {
			        Thread.sleep(10);
			    } catch (InterruptedException e) {
			        e.printStackTrace();
			    }
			}

            // Temps de pause pour le pigeon.
            try {
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
	}



}
