package model;

import model.Enum.ActionType;;

public interface UpdateEventListener {
	void onSquareUpdate(Item item, ActionType actiontype);
}
