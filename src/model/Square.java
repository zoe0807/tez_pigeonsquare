package model;

import java.util.ArrayList;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import model.Enum.ActionType;

public class Square {
    private Integer xSize;
    private Integer ySize;
    private final ArrayList<Pigeon> pigeons;
    private final ArrayList<Food> foods;
    private final ArrayList<Repulsive> repulsives;
    public final ReadWriteLock verrou = new ReentrantReadWriteLock();
    
    private UpdateEventListener updateListener;

    public Square(Integer xSize, Integer ySize){
        this.xSize = xSize;
        this.ySize = ySize;
        pigeons = new ArrayList<>();
        foods = new ArrayList<>();
        repulsives = new ArrayList<>();
    }
    

    public Integer getxSize() {
		return xSize;
	}

	public Integer getySize() {
		return ySize;
	}

	public ArrayList<Food> getFoods(){
    	return foods;
    }
    /**
     * Méthode pour savoir si il existe de la nourriture placé sur le square
     * @return boolean
     */
    public boolean isFoodHere(){
        if(foods.size() != 0)
            return true;
        else
            return false;
    }

    /**
     * Méthode pour savoir si il existe des repulsifs placé sur le square
     * @return
     */
    public boolean isRepulsiveHere(){
        if(repulsives.size() != 0)
            return true;
        else
            return false;
    }
    
    public Item addPigeon() {
    	Item item;
    	Coords position = new Coords(400, 400);
    	// Faire condition ici si on veut definir un nb max de pigeon sur le terrain
    	//verrou.writeLock().lock();
    	item = new Pigeon(position, this);
    	pigeons.add((Pigeon) item);
    	update(item, ActionType.Create);
    	//verrou.writeLock().unlock();
    	return item;
    }
    
    public Item addPigeon(Coords position) {
    	Item item;
    	// Faire condition ici si on veut definir un nb max de pigeon sur le terrain
    	//verrou.writeLock().lock();
    	item = new Pigeon(position, this);
    	pigeons.add((Pigeon) item);
    	update(item, ActionType.Create);
    	//verrou.writeLock().unlock();
    	return item;
    }
    
    
    public Item addFood(Coords position) {
    	// Faire condition ici si on veut definir un nb max de food sur le terrain
    	//verrou.writeLock().lock();
    	Item food = new Food(position, this);
    	foods.add((Food)food);
    	update(food, ActionType.Create); // On previent l'interface qu'on ajoute de la nourriture
    	//verrou.writeLock().unlock();
    	return food;
    }
    
    public Item addRepulsive(Coords position) {
    	Item repulsive = new Repulsive(position, 2000, this);
    	repulsives.add((Repulsive) repulsive);
    	update(repulsive, ActionType.Create);
    	return repulsive;
    }

    public void remove(Food food){
    	//verrou.writeLock().lock();
		food.stopFreshTime();
        foods.remove(food);
        food.stopRunning();
        update(food, ActionType.Delete);
        //verrou.writeLock().unlock();
        // stop thread
        //printList(foods);
    }
    public void printList(ArrayList<Food> foods2) {
    	for(Food val : foods2) {
    		System.out.println(val.isFresh());
    	}
    }

    public void remove(Repulsive repulsive){
    	verrou.writeLock().lock();
        repulsives.remove(repulsive);
        update(repulsive, ActionType.Delete);
        verrou.writeLock().unlock();
    }
    
    /** 
     * Fonction appel�e lorsque la nourriture n'est plus fraiche
     * @param food
     */
    public void foodNoLongerFresh(Food food) {
    	verrou.writeLock().lock();
    	update(food, ActionType.Update);
    	verrou.writeLock().unlock();
    }


    /**
     * Méthode qui retourne le réplsif le plus proche du pigeon placé en paramètre
     * @param pigeon
     * @return
     * @throws Exception
     */
    public Repulsive getCloseRepulsive(Pigeon pigeon) throws Exception {
        if (isRepulsiveHere()) {
            Integer i, bestIndex = 2147483647;
            double bestDistance = Double.MAX_VALUE, actualDistance;
            for (i = 0; i < repulsives.size(); i++) {
                actualDistance = pigeon.distance(pigeon, repulsives.get(i).getCoords().getX(), repulsives.get(i).getCoords().getY());
                if (actualDistance < bestDistance) {
                    bestDistance = actualDistance;
                    bestIndex = i;
                }
            }
            return repulsives.get(bestIndex);
        } else
            throw new Exception("Il n'y a pas de bombe sur le square");
    }

    /**
     * Méthode qui retourne la nourriture la plus proche du pigeon passé en paramètre
     */
    public Food getCloseFood(Pigeon pigeon) throws Exception {
        if(isFoodHere()) {
            Integer i, bestIndex = 2147483647;
            double bestDistance = Double.MAX_VALUE, actualDistance;
            for (i = 0; i < foods.size(); i++) { // Peut provoquer exception "IndexOutOfBoundsException"
                actualDistance = pigeon.distance(pigeon, foods.get(i).getCoords().getX(), foods.get(i).getCoords().getY());
                if (actualDistance < bestDistance) {
                    bestDistance = actualDistance;
                    bestIndex = i;
                }
            	
            }
            return foods.get(bestIndex);
        }else {
            throw new Exception("Il n'y a pas de nourriture sur le square");
        }
    }
    
    /**
	 * Fonction qui permet de mettre � jour l'interface graphique
	 */
	public void update(Item item, ActionType actiontype) {
		
		if (updateListener != null) {
			updateListener.onSquareUpdate(item, actiontype);
		}
	}
	
	public void setUpdateEventListener(UpdateEventListener updateListener) {
		this.updateListener = updateListener;
	}
}
