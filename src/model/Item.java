package model;

import javafx.scene.image.ImageView;

public abstract class Item extends Thread {
    private Coords coords;
    private ImageView imageView;
    protected boolean isRunning;
    
    public Item() {
    	this.isRunning = true;
    }

    public Coords getCoords() {
        return this.coords;
    }

    public void setCoords(Coords coords) {
        this.coords = coords;
    }

    public ImageView getImageView() {
    	return this.imageView;
    }
    
    public void setImageView(ImageView imageView) {
    	this.imageView = imageView;
    }
    
    // Calcul la distance entre le pigeon et l'objet aux coordonnées x et y
    public double distance(Pigeon pigeon, Integer x, Integer y){
        Integer pigeonx = pigeon.getCoords().getX();
        Integer pigeony = pigeon.getCoords().getY();
        Integer itemx = x;
        Integer itemy = y;

        // cherche la distance (resp) en x et en y entre le pigeon et l'objet
        double distx = itemx - pigeonx;
        double disty = itemy - pigeony;

        // calcul la distance la plus courte entre le pigeon et l'objet
        return Math.sqrt(distx*distx + disty*disty);
    }
    
    // Arreter le thread
    public void stopRunning() {
    	this.isRunning = false;
    }
}
