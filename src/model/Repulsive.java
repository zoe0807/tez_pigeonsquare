package model;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Repulsive extends Item implements ActionListener {

    private Timer apparitionTime = null;
    private Square square = null;

    public Repulsive(Coords coords, int apparitionTime, Square square){
        this.apparitionTime = new Timer(apparitionTime, this);
        this.setCoords(coords);
        this.square = square;
        this.apparitionTime.start();
    }


    // Supprime le repulsif du sol
    @Override
    public void actionPerformed(ActionEvent e) {
    	System.out.println("FIN");
        square.remove(this);
        this.apparitionTime.stop();
    }


	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}
}
