package model;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Food extends Item implements ActionListener {
	
	private Timer freshTime;
	private boolean fresh;
	private Square square;
	
	public Food(Coords coords, Square square) {
		this.setCoords(coords);
		this.freshTime = new Timer(5000, this);
		this.fresh = true;
		this.square = square;
		this.fresh = true;
		this.freshTime.start();
	}

	public void setFresh(boolean isFresh) {
		this.fresh = isFresh;
	}
	public boolean isFresh() {
		return fresh;
	}

	public void manger(){
		square.remove(this);
	}
	
	public boolean inSquare() {
		ArrayList<Food> foods = square.getFoods();
		return(foods.contains(this));
	}
	
	public void stopFreshTime() {
		freshTime.stop();
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(inSquare()) { // on verifie que la nourriture n'a pas �t� mang�e
			fresh = false;
			square.foodNoLongerFresh(this);
			System.out.println("NON MANGEE");
		}
		else {
			System.out.println("MANGEE!!");
		}
		square.remove(this);
	}

	@Override
	public void run() {
		System.out.println("{FOOD} Thread is running");	
	}
}
