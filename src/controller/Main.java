package controller;
	
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import model.Coords;
import model.Item;
import model.Square;
import view.UIManager;


public class Main extends Application {
	public static final Pane root = new Pane();
	private static Square square;
	
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("TP2 - Pigeon Square 2");
		primaryStage.setResizable(false);
		addButtons();
		defineMouseEvents();
		Scene scene = new Scene(root, 800, 800);
		//scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	public void addButtons() {
		Button addPigeon = new Button("Ajouter un pigeon");
		Label labelPos = new Label("Position : ");
		Label labelSpeed = new Label("Vitesse : ");
		TextField fieldPosX = new TextField("400");
		TextField fieldPosY = new TextField("400");
		ComboBox<String> speedList = new ComboBox<String>();
		
		speedList.getItems().addAll("Lent", "Normal", "Rapide");
		
		addPigeon.setLayoutX(5);
		addPigeon.setLayoutY(7);
		
		labelPos.setLayoutX(170);
		labelPos.setLayoutY(14);
		
		fieldPosX.setLayoutX(250);
		fieldPosX.setLayoutY(7);
		fieldPosX.setPrefWidth(50);
		
		fieldPosY.setLayoutX(310);
		fieldPosY.setLayoutY(7);
		fieldPosY.setPrefWidth(50);
		
		labelSpeed.setLayoutX(370);
		labelSpeed.setLayoutY(14);
		
		speedList.setLayoutX(440);
		speedList.setLayoutY(7);
		speedList.getSelectionModel().select(1); //selectFirst();

		addPigeon.setOnAction(e -> { // Creation d'un pigeon
			Item item = null;
			item = square.addPigeon(new Coords(Integer.parseInt(fieldPosX.getText()), Integer.parseInt(fieldPosY.getText())));
			item.start();
		});
		
		/* Ajouts des elements a l'interface */
		root.getChildren().add(addPigeon);
		root.getChildren().add(labelPos);
		root.getChildren().add(fieldPosX);
		root.getChildren().add(fieldPosY);
		root.getChildren().add(labelSpeed);
		root.getChildren().add(speedList);
		
	}
	
	public void defineMouseEvents() {
		root.setOnMouseClicked(click -> {
			Item item = null;
			
			Coords position = new Coords((int)click.getX(), (int)click.getY()); // position du clic
			
			if(click.getButton() == MouseButton.PRIMARY) { // clic gauche
				item = square.addFood(position); // ajout de la nourriture
				item.start(); // demarrage du thread
			}
			else if(click.getButton() == MouseButton.SECONDARY) { // clic droit
				item = square.addRepulsive(position);
				item.start();
			}
		});
	}
	
	public static void main(String[] args) {
		square = new Square(800, 800); // Creation du square
		UIManager uimanager = new UIManager(square);
		launch(args);
	}
}
